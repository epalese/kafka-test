import aiokafka
import asyncio
import unittest
import consumer_service
import consumer_model


class TestInitConsumer(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        super(TestInitConsumer, cls).setUpClass()
        cls.topic = 'kafka_test1_topic'     # 'kafka_test_' + str(int(time.time())) + '_topic'
        cls.loop = asyncio.get_event_loop()
        cls.producer = aiokafka.AIOKafkaProducer(
            loop=cls.loop,
            bootstrap_servers='localhost:9092',
            acks='all'
        )
        consumer_service.consumer = consumer_model.LoggerConsumer(
            loop=cls.loop,
            bootstrap_servers='localhost:9092',
            group_id='TEST1',
            auto_offset_reset='earliest',
            topic=cls.topic)
        cls.loop.run_until_complete(cls.producer.start())
        # generate 3 messages
        for i in range(3):
            f = cls.loop.run_until_complete(cls.producer.send(TestInitConsumer.topic, key=str(i).encode(), value="test".encode()))
            result_sent = cls.loop.run_until_complete(asyncio.gather(f))
            print(result_sent)

    @classmethod
    def tearDownClass(cls):
        super(TestInitConsumer, cls).tearDownClass()
        cls.loop.run_until_complete(cls.producer.stop())
        cls.loop.run_until_complete(consumer_service.consumer.stop())
        cls.loop.stop()

    def test_init_state(self):
        """
        Test that after calling :meth:`AbstractConsumer.init()` the state is 'ready'.
        """
        loop = asyncio.get_event_loop()
        loop.run_until_complete(consumer_service.consumer.init())
        assert consumer_service.consumer.get_state() == 'ready'

    def test_partition_offset_commit(self):
        """
        Test that :meth:`AbstractConsumer.process()` advances the committed offset
        after processing the messages.
        In this test case we are simulating the reception of a single message, so the
        offset after the commit should be one more than the previous one.
        :return:
        """
        loop = asyncio.get_event_loop()
        kconsumer = consumer_service.consumer.get_kafka_consumer()
        tps = kconsumer.assignment()
        assert len(tps) == 1
        tp = tps.pop()
        # last_committed_offset = loop.run_until_complete(kconsumer.committed(tp))
        next_record_offset = loop.run_until_complete(kconsumer.position(tp))
        # last_offset_dict = loop.run_until_complete(kconsumer.end_offsets([tp]))
        print("reading 1 record...")
        result_read = loop.run_until_complete(consumer_service.consumer.get_kafka_consumer().getmany(timeout_ms=1000, max_records=1))
        # only 1 TopicPartition returned
        assert len(result_read) == 1
        records = None
        for key in result_read.keys():
            records = result_read[key]
        assert len(records) == 1
        record = records[0]
        print(record)
        assert record.topic == TestInitConsumer.topic
        assert record.partition == 0
        loop.run_until_complete(consumer_service.consumer.process(result_read))
        tps = kconsumer.assignment()
        assert len(tps) == 1
        tp = tps.pop()
        last_committed_offset = loop.run_until_complete(kconsumer.committed(tp))
        assert last_committed_offset == next_record_offset + 1


def suite():
    tests = ['test_state_endpoint', 'test_partition_offset']
    return unittest.TestSuite(map(TestInitConsumer, tests))


if __name__ == "__main__":
    unittest.main()
