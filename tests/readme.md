Testing
=======
The tests provided are by no mean exhaustive. The intent is to show how I would have approached testing in a scenario with:  
- distributed components  
- asynchrnous paradigm  

Note:  
I have tried to also test the endpoints using [Sanic test_client](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet#links) but apparently
it forces the async loop to close after each request. I need more time to investigate this feature.   

Setup
-----
Requirements:  
- virtualenv and virtualenv-wrapper  
- docker  

Create a virtual environment and install the requirements:  
```bash
$ cd kafka-test
$ pip install -r tests/requirements.txt
```

Start the docker containers:  
```bash
$ cd kafka-test
$ docker-compose -f docker-compose-test.yml up -d
$ docker-compose -f docker-compose-test.yml down --volumes --remove-orphans  
```

To run the tests:
```bash
$ cd kafka-test
$ export PYTHONPATH=./services/consumer:./services/producer:./services/data_generator
$ python -m unittest discover -s tests/
```

To restart docker containers:
```bash
$ docker-compose -f docker-compose-test.yml down --volumes --remove-orphans
$ docker-compose -f docker-compose-test.yml up
```