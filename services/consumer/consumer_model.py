import aiokafka
import asyncio
import motor.motor_asyncio
import logging

logger = logging.getLogger(__name__)


class AbstractConsumer(object):
    """
    AbstractConsumer provides the code to initialize a Kafka consumer and keep its state.
    Concrete classes have to provide the implementation for the method :meth:`process`. This methid determines how
    messages coming from Kafka are processed. 
    """
    def __init__(self, loop, bootstrap_servers, group_id, auto_offset_reset, topic):
        """
        Set the common attributes that all subclasses have to perform.
            * Kafka consumer: A Kafka consumer is an instance of :class:`aiokafka.AIOKafkaConsumer`.
            * define a `state` attribute: initial value for the state attribute is 'stopped'.

        :param loop: Asyncio loop used by kafka consumer for its execution.
        :type loop: asyncio.loop
        :param bootstrap_servers: List of Kafka brokers.
        :type bootstrap_servers: string
        :param group_id: Group id that the consumer will join.
        :type group_id: string
        :param auto_offset_reset: set behaviour in case the offset position is either undefined or incorrect (see aiokafka documentation)
        :type auto_offset_reset: string
        :param topic: Topic to subscribe to
        :type topic: string
        """
        self.consumer = aiokafka.AIOKafkaConsumer(
            loop=loop, bootstrap_servers=bootstrap_servers, group_id=group_id,
            auto_offset_reset=auto_offset_reset, enable_auto_commit=False)
        self.topic = topic
        self.state = 'stopped'

    def get_state(self):
        """
        Return the current state of the consumer ('running' or 'stopped')
        :return: consumer state
        :rtype: string
        """
        return self.state

    def set_state(self, new_state):
        """
        Set the state of the consumer.
        :param new_state: new state; valid values are 'running' and 'stopped
        :return: None
        """
        self.state = new_state

    def get_kafka_consumer(self):
        """
        Return the Kakfa consumer (instance of :class:`aiokafka.AIOKafkaConsumer`)
        :return: Kafka consumer
        :rtype: :class:`aiokafka.AIOKafkaConsumer
        """
        return self.consumer

    async def stop(self):
        """
        Close consumer. If connections have been opened in :meth:`setup_dependencies()`
        in the child class this method can be overloaded (taking care of calling the
        method of its parent).
        :return:
        """
        self.consumer.stop()

    async def setup_dependencies(self):
        """
        Generic step to setup all dependencies required (eg. connection to MongoDB).
        :return: None
        """
        raise NotImplementedError()

    async def connect_to_cluster(self, retries=0):
        """
        Wait for the cluster to be available and connect the consumer to it.
        A `retries` parameter specify how many times the service will try to connect to the cluster.
        If it fails the method returns False.
        With `retries = 0` the service will try to connect to the cluster until it is available.
        :param retries: Number of times to try and contact the cluster if it is not available
        :return: True if the connection is established, False otherwise.
        :rtype: bool
        """
        connected = False
        tried = 0
        while (not connected) and (tried <= retries):
            try:
                await self.consumer.start()
                connected = True
            except aiokafka.errors.KafkaError as err:
                if retries > 0:
                    tried += 1
                await asyncio.sleep(5)
        return connected

    async def subscribe_to_topic(self, retries=0):
        """
        Subscribe the consumer to the topic specified by `AbstractConsumer.topic`.
        The method wait for the topic to be available before subscribing.  It will
        wait indefinitely unless a value greater than zero is specified for the
        parameter `retries`.
        :param retries: The number of times to check for the existence of the topic. A value equals to 0 will allow
        the service to wait for the topic indefinitely until it finds it.
        :return: True if the subscription occurs, False otherwise.
        """
        topics = []
        tried = 0
        # Wait for topic to be created.
        while (self.topic not in topics) and (tried <= retries):
            logger.info('Checking for topic {topic}...'.format(topic=self.topic))
            topics = await self.consumer.topics()
            if self.topic not in topics:
                logger.debug('Topic {topic} not found. Sleeping and retrying...'.format(topic=self.topic))
                if retries > 0:
                    tried += 1
                await asyncio.sleep(3)
            else:
                logger.info("Topic {topic} found.".format(topic=self.topic))
                logger.info("Subscribing to {topic}...".format(topic=self.topic))
                self.consumer.subscribe(self.topic)
        return self.topic in topics

    async def get_partition_assignment(self, retries=0):
        """
        Get the partition assigned to the current consumer. It waits until at least one partition has been
        assigned. The check for partition assignment is perform indefinitely unless a value different than
        zero is specified for the parameter `retries`.
        :param retries: how many times to test for partition assignment.
        :return: a tuple containing the assigned partitions.
        :rtype: tuple
        """
        assigned_partitions = ()
        tried = 0
        while (len(assigned_partitions) == 0) and (tried <= retries):
            logger.info("Waiting for partitions to be assigned to the consumer...")
            assigned_partitions = self.consumer.assignment()
            if len(assigned_partitions) == 0:
                if retries > 0:
                    tried += 1
                await asyncio.sleep(3)
        return assigned_partitions

    async def setup_offset(self, assigned_partition):
        """
        Actions to setup offset per each assigned partition before data consumption starts.
        :param assigned_partition: partition assigned to the consumer
        :type assigned_partition: :class:`aiokafka.TopicPartition`
        :return: None
        """
        raise NotImplementedError()

    async def init(self):
        """
        Performs the steps to initialize the consumer:
        * `setup_dependencies()`: set up any additional dependencies (eg. connections to dbs, etc.); abstract method
        * `connect_to_cluster()`: wait for and connect to the Kafka cluster
        * `subscribe_to_topic()`: wait for and subscribe to topic
        * `get_partition_assignment()`: wait for and get partitions assigned to the consumer
        * `setup_offset(assigned_partition)`: method called for each assigned partition; abstract method
        :return:
        """
        self.state = 'initializing'
        logger.info('Executing setup_dependencies step...')
        await self.setup_dependencies()

        logger.info('Connecting to Kafka cluster...')
        connected = await self.connect_to_cluster()
        logger.info('Connected to Kafka cluster? {connected}'.format(connected=connected))

        logger.info('Subscribing to topic {topic}...'.format(topic=self.topic))
        subscribed = await self.subscribe_to_topic()
        logger.info('Subscribed to {topic}? {subscribed}'.format(topic=self.topic, subscribed=subscribed))

        logger.info('Getting assigned partitions...')
        assigned_partitions = await self.get_partition_assignment()
        logger.info("Assigned partitions: {assigned_partitions}".format(assigned_partitions=assigned_partitions))

        logger.info("Executing setup_offset...")
        for ap in assigned_partitions:
            await self.setup_offset(ap)
        self.state = 'ready'

    async def process(self, data):
        """
        Implement the processing logic for records coming from Kafka cluster.
        :param data: dictionary containing TopicPartition: [list of Kafka records]
        :return: None
        """
        raise NotImplementedError()

    async def run(self):
        """
        This method first connects the Kafka cluster and waits for:
            * the cluster to be available
            * topic to be ready
            * partitions to be assigned to the consumer
        When the cluster is ready an infinite loop is started that checks for new messages in the subscribed topic.
        Listening for new messages is timed out every one second in order to allow to force a 'stopped' state (via
        :meth:`set_state(new_state)` method).
        :return: None
        """
        await self.init()
        logger.info("Start processing messages...")
        self.state = 'running'
        while True:
            if self.state == 'running':
                try:
                    data = await self.consumer.getmany(timeout_ms=1000, max_records=1)
                    await self.process(data)
                except aiokafka.errors.KafkaError as err:
                    logger.debug("ERROR: ", err)
            else:
                await asyncio.sleep(1)


class LoggerConsumer(AbstractConsumer):
    """
    Log messages to the standard output.
    """

    def __init__(self, *args, **kwargs):
        logger.debug("Initializating LoggerConsumer instance...")
        super(LoggerConsumer, self).__init__(*args, **kwargs)

    async def setup_dependencies(self):
        pass

    async def setup_offset(self, assigned_partition):
        committed = await self.consumer.committed(assigned_partition)
        logger.info("TopicPartition {tp}: last committed message = {committed}".format(
            tp=assigned_partition, committed=committed))
        if committed is None:
            offset = 0
        else:
            offset = committed
        self.consumer.seek(assigned_partition, offset)

    async def process(self, data):
        for tp, messages in data.items():
            for msg in messages:
                logger.debug(
                    "[{topic}:{partition:d}:{offset:d}] key={key}; value={value}; timestamp={timestamp}".format(
                        topic=msg.topic, partition=msg.partition, offset=msg.offset,
                        key=msg.key, value=msg.value, timestamp=msg.timestamp)
                )
                print(
                    "[{topic}:{partition:d}:{offset:d}] key={key}; value={value}; timestamp={timestamp}".format(
                        topic=msg.topic, partition=msg.partition, offset=msg.offset,
                        key=msg.key, value=msg.value, timestamp=msg.timestamp)
                )
                await self.consumer.commit({tp: messages[-1].offset + 1})
                logger.debug("Committed topic={topic} partition={partition:d} offset={offset:d}".format(
                    topic=messages[-1].topic, partition=messages[-1].partition, offset=messages[-1].offset + 1
                ))


class MongoConsumer(AbstractConsumer):
    """
    Log messages to MongoDB.
    """

    def __init__(self, mongo_url, *args, **kwargs):
        logger.debug("Initializating MongoConsumer instance...")
        super(MongoConsumer, self).__init__(*args, **kwargs)
        self.mongo_url = mongo_url
        self.mongo_client = None
        self.mongo_collection = None

    async def setup_dependencies(self):
        logger.debug("Connecting to MongoDB {url}".format(url=self.mongo_url))
        self.mongo_client = motor.motor_asyncio.AsyncIOMotorClient(self.mongo_url)
        self.mongo_collection = self.mongo_client.kafka_test['messages']

    async def setup_offset(self, assigned_partition):
        committed = await self.consumer.committed(assigned_partition)
        logger.info("TopicPartition {tp}: last committed message stored in kafka = {committed}".format(
            tp=assigned_partition, committed=committed))
        cursor = self.mongo_collection.find({"partition": assigned_partition.partition}).sort([("offset", -1)]).limit(1)
        r = await cursor.to_list(length=1)
        last_mongo_offset = None
        if r and len(r) > 0:
            last_mongo_offset = r[0]['offset']
            logger.info("TopicPartition {tp}: last committed message stored in mongo = {last_mongo_offset}".format(
                tp=assigned_partition, last_mongo_offset=last_mongo_offset))
        if last_mongo_offset is None:
            offset = 0
        else:
            offset = last_mongo_offset + 1
        self.consumer.seek(assigned_partition, offset)

    async def process(self, data):
        for tp, messages in data.items():
            for msg in messages:
                logger.debug(
                    "[{topic}:{partition:d}:{offset:d}] key={key}; value={value}; timestamp={timestamp}".format(
                        topic=msg.topic, partition=msg.partition, offset=msg.offset,
                        key=msg.key, value=msg.value, timestamp=msg.timestamp)
                )
                doc = {
                    'topic': msg.topic,
                    'partition': msg.partition,
                    'offset': msg.offset,
                    'key': msg.key.decode(),
                    'value': msg.value.decode()
                }
                result = None
                while not result:
                    try:
                        result = await self.mongo_collection.insert(doc)
                    except Exception as e:
                        logger.error("ERROR insert message: {e}".format(e=e))
                        logger.error("Retrying after 5 seconds...".format(e=e))
                        asyncio.sleep(5)

                await self.consumer.commit({tp: messages[-1].offset + 1})
                logger.debug("Committed topic={topic} partition={partition:d} offset={offset:d}".format(
                    topic=messages[-1].topic, partition=messages[-1].partition, offset=messages[-1].offset + 1
                ))
