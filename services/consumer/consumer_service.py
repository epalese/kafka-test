import argparse
import asyncio
import logging
import os
from consumer_model import LoggerConsumer, MongoConsumer
from sanic import Sanic
from sanic import response
from signal import signal, SIGINT


BROKERS = os.environ.get('KAFKA_TEST_BROKERS')
TOPIC = os.environ.get('KAFKA_TEST_TOPIC')
SERVICE_PORT = os.environ.get('SERVICE_PORT')
SERVICE_OUTPUT = os.environ.get('SERVICE_OUTPUT')
GROUP_ID = os.environ.get('CONSUMER_GROUP_ID')
MONGO_URL = os.environ.get('MONGO_URL')

app = Sanic(__name__)
consumer = None
logging_format = "[%(asctime)s] %(process)d-%(levelname)s %(module)s::%(funcName)s():l%(lineno)d: %(message)s"
logging.basicConfig(format=logging_format, level=logging.ERROR)
logger = logging.getLogger(__name__)


@app.route("/state", methods=['POST'])
async def set_state(request):
    """
    Set the current state of the service. The response must contain a JSON document with the following attribute:
    * state: accepted value are 'running' or 'stopped'.

    :param request:
    :return:
    """
    global consumer
    previous_state = consumer.get_state()
    logger.debug("state = " + previous_state)
    _state = request.json['state']
    logger.debug("Setting state to " + str(_state))
    consumer.set_state(_state)
    return response.json({"previous_state": previous_state, "new_state": _state}, status=200)


@app.route("/state", methods=['GET'])
async def get_state(request):
    """
    Get the current state of the service.
    The JSON document returned contains the current state of the service (running or stopped) and
    for each couple (Topic, Partition) assigned to the consumer:
    * offset of the last committed message
    * offset of the next record (message) that will be returned
    * offset of the most recent record in the partition

    :param request:
    :return:
    """
    global consumer
    state = consumer.get_state()
    kconsumer = consumer.get_kafka_consumer()
    logger.info("get state = " + str(state))
    tps = kconsumer.assignment()
    logger.debug("Assigned partitions: {tps}".format(tps=tps))
    info = []
    for tp in tps:
        last_committed_offset = await kconsumer.committed(tp)
        next_record_offset = await kconsumer.position(tp)
        last_offset_dict = await kconsumer.end_offsets([tp])
        if last_offset_dict:
            last_offset = last_offset_dict.get(tp)
        info.append({
            "topic": tp.topic,
            "partition": tp.partition,
            "last_committed_offset": last_committed_offset,
            "next_record_offset": next_record_offset,
            "last_offset": last_offset})
    return response.json({"state": state, "tp_state": info}, status=200)

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="Consumer.")
    parser.add_argument("-g", "--groupid", action="store", help="Group id of the consumer.")
    parser.add_argument("-p", "--port", action="store", help="Port used to listen for requests.")
    parser.add_argument("-v", "--verbose", action="store_true", help="Increase output verbosity to debug level.")
    parser.add_argument("-o", "--output", action="store", help="Consumer output type. Accepted values: log, db")
    args = parser.parse_args()

    if args.verbose:
        logging.getLogger('consumer_model').setLevel(logging.DEBUG)
        logger.setLevel(logging.DEBUG)
    else:
        logging.getLogger('consumer_model').setLevel(logging.INFO)
        logger.setLevel(logging.INFO)

    if BROKERS:
        logger.info("Using BROKERS defined in env variables: {brokers}".format(brokers=BROKERS))
        brokers = BROKERS.split(',')
    else:
        brokers = 'localhost:9092'
        logger.info("Using default value for BROKERS: {brokers}".format(brokers=brokers))

    if TOPIC:
        logger.info("Using TOPIC defined in env variables: {topic}".format(topic=TOPIC))
        topic = TOPIC
    else:
        topic = 'kafka_test_topic'
        logger.info("Using default value for TOPIC: {topic}".format(topic=topic))

    if args.port:
        logger.info("Using SERVICE_PORT defined via parameters: {service_port}".format(service_port=args.port))
        service_port = int(args.port)
    elif SERVICE_PORT:
        logger.info("Using SERVICE_PORT defined in env variables: {service_port}".format(service_port=SERVICE_PORT))
        service_port = SERVICE_PORT
    else:
        service_port = 10000
        logger.info("Using default value for SERVICE_PORT: {service_port}".format(service_port=service_port))

    if args.output and args.output in ('log', 'db'):
        output = args.output
        logger.info("Using SERVICE_OUTPUT defined via parameters: {service_output}".format(service_output=output))
    elif SERVICE_OUTPUT and SERVICE_OUTPUT in ('log', 'db'):
        output = SERVICE_OUTPUT
        logger.info("Using SERVICE_OUTPUT defined in env variables: {service_output}".format(service_output=output))
    else:
        output = 'log'
        logger.info("Using default value for SERVICE_OUTPUT: {service_output}".format(service_output=output))

    if args.groupid:
        group_id = args.groupid
        logger.info("Using GROUP_ID defined via parameters: {group_id}".format(group_id=group_id))
    elif GROUP_ID:
        group_id = GROUP_ID
        logger.info("Using GROUP_ID defined in env variables: {group_id}".format(group_id=group_id))
    else:
        group_id = 'default_group'
        logger.info("Using default value for GROUP_ID: {group_id}".format(group_id=group_id))

    if MONGO_URL:
        mongo_url = MONGO_URL
        logger.info("Using MONGO_URL defined in env variables: {mongo_url}".format(mongo_url=mongo_url))
    else:
        mongo_url = 'mongodb://kafkauser:dummy@localhost:27017/kafka_test'
        logger.info("Using default value for MONGO_URL: {mongo_url}".format(mongo_url=mongo_url))

    server = app.create_server(host="0.0.0.0", port=service_port, access_log=False)
    task = asyncio.ensure_future(server)
    loop = asyncio.get_event_loop()

    if output == 'log':
        consumer = LoggerConsumer(loop=loop, bootstrap_servers=brokers, group_id=group_id, auto_offset_reset='latest', topic=topic)
    elif output == 'db':
        consumer = MongoConsumer(loop=loop, mongo_url=mongo_url, bootstrap_servers=brokers, group_id=group_id, auto_offset_reset='latest', topic=topic)
    loop.create_task(consumer.run())
    signal(SIGINT, lambda s, f: loop.stop())
    try:
        loop.run_forever()
    except Exception:
        loop.create_task(consumer.stop())
        loop.stop()
