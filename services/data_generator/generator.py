import argparse
import json
import logging
import os
import random
import requests
import time
from multiprocessing import Process
from signal import signal, SIGINT


PRODUCER_ENDPOINT = os.environ.get('PRODUCER_ENDPOINT')

logging.basicConfig(format='%(asctime)s %(message)s', datefmt='%Y-%m-%d %I:%M:%S', level=logging.ERROR)
logger = logging.getLogger(__name__)
run = True


def requests_generator(endpoint, wait_time, num_messages=0):
    def _generate():
        id = random.randint(1, 1000000)
        payload = "Payload for id = {id}".format(id=id)
        data = json.dumps({'id': id, 'payload': payload})
        r = None
        while not r and run:
            try:
                r = requests.post(endpoint, data=data)
            except Exception as e:
                logger.error("ERROR sending msg = {msg} -> {e}".format(msg=data, e=e))
                time.sleep(wait_time / 1000.0)
        logger.debug("[{status_code}] msg = {msg} -> {response}".format(status_code=r.status_code, msg=data, response=r.text))
        time.sleep(wait_time / 1000.0)

    if num_messages > 0:
        while num_messages > 0:
            _generate()
            num_messages -= 1
    else:
        while run:
            _generate()


def stop():
    global run
    run = False


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="Requests generator.")

    parser.add_argument("-e", "--endpoint", action="store", help="Endpoint to call")
    parser.add_argument("-m", "--messages", action="store", help="(Optional) Number of messages per process to send.")
    parser.add_argument("-p", "--processes", action="store", help="Number of processes to spin up")
    parser.add_argument("-w", "--wait", action="store", help="Time to wait (ms) between each request")
    parser.add_argument("-v", "--verbose", action="store_true", help="Increase output verbosity to debug level")

    args = parser.parse_args()
    if args.endpoint:
        endpoint = args.endpoint
    elif PRODUCER_ENDPOINT:
        logger.info("Using PRODUCER_ENDPOINT defined in env variables: {endpoint}".format(endpoint=PRODUCER_ENDPOINT))
        endpoint = PRODUCER_ENDPOINT
    else:
        endpoint = 'localhost:8080/publish'
        logger.info("Using default value for BROKERS: {endpoint}".format(endpoint=endpoint))

    if args.processes:
        num_processes = int(args.processes)
    else:
        processes = 1

    if args.wait:
        wait_time = int(args.wait)
    else:
        wait_time = 1000

    if args.messages:
        num_messages = int(args.messages)
    else:
        num_messages = 0

    if args.verbose:
        logger.setLevel(logging.DEBUG)
    else:
        logger.setLevel(logging.INFO)

    signal(SIGINT, lambda s, f: stop())

    processes = []
    for i in range(num_processes):
        logger.info("Starting process #{}".format(i))
        p = Process(name="#{}".format(i), target=requests_generator, args=(endpoint, wait_time, num_messages))
        processes.append(p)
        p.start()

    for p in processes:
        p.join()
        logger.info("Process {name} stopped".format(name=p.name))
