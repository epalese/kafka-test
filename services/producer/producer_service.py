import aiokafka
import argparse
import asyncio
import logging
import os
from sanic import Sanic
from sanic import response
from signal import signal, SIGINT


BROKERS = os.environ.get('KAFKA_TEST_BROKERS')
TOPIC = os.environ.get('KAFKA_TEST_TOPIC')
SERVICE_PORT = os.environ.get('SERVICE_PORT')

app = Sanic(__name__)
queue = asyncio.Queue()
state = 'stopped'
logging_format = "[%(asctime)s] %(process)d-%(levelname)s %(module)s::%(funcName)s():l%(lineno)d: %(message)s"
logging.basicConfig(format=logging_format, level=logging.ERROR)
logger = logging.getLogger(__name__)


async def _process():
    global state
    state = 'initializing'
    logger.info('Connecting to Kafka cluster...')
    connected = False
    while not connected:
        try:
            await producer.start()
            connected = True
        except aiokafka.errors.KafkaError as err:
            await asyncio.sleep(5)

    # Wait for topic to be created
    partitions = None
    while partitions is None:
        logger.info('Checking for topic {topic}...'.format(topic=topic))
        try:
            partitions = await producer.partitions_for(topic)
            logger.info("Found topic with partitions {partitions}".format(partitions=partitions))
        except aiokafka.errors.KafkaError as err:
            await asyncio.sleep(1)

    logger.info("Topic {topic} found.".format(topic=topic))
    state = 'running'
    logger.info('Starting producer...')
    while True:
        if state == 'running':
            try:
                logger.debug("Waiting for message in the queue")
                message = await asyncio.wait_for(queue.get(), 1)
                logger.debug("message: " + str(message))
                id = message['id']
                payload = message['payload']
                f = await producer.send(topic, key=str(id).encode(), value=payload.encode())
                response = await f
                logger.info("result: " + str(response))
            except asyncio.TimeoutError as e:
                # TODO: here a decision has to be taken if to resend the message or not. Resending could cause duplicates
                pass
            except Exception as e:
                logger.error("ERROR in processing the message: {e}".format(e=e))
        else:
            await asyncio.sleep(1)


@app.route("/publish", methods=['POST'])
async def publish(request):
    """
    This endpoint is called by data generators to publish new data.
    The content of the request must be a json document with:
    * id: an integer
    * payload: generic content

    The response object will one of the following JSON objects:
    * {"message": "ok"}
    * {"message": "queued"}
    * {"message": "error: queue full"}

    :param request:
    :return:
    """
    message = request.json
    logger.info("Received message " + str(message))
    try:
        await queue.put(message)
    except e as QueueFull:
        return response.json({"message": "error: queue full"}, status=503)
    else:
        if state == 'running':
            return response.json({"message": "ok"}, status=200)
        else:
            return response.json({"message": "queued"}, status=200)


@app.route("/state", methods=['POST'])
async def set_state(request):
    """
    Set the current state of the service. The response must contain a JSON document with the following attribute:
    * state: accepted value are 'running' or 'stopped'.

    :param request:
    :return:
    """
    global state
    logger.debug("state = " + str(state))
    _state = request.json['state']
    logger.debug("Setting state to " + str(_state))
    previous_state = state
    state = _state
    return response.json({"previous_state": previous_state, "new_state": state}, status=200)


@app.route("/state", methods=['GET'])
async def get_state(request):
    """
    Return the current state of the producer (stopped or running).

    :param request:
    :return:
    """
    global state
    logger.debug("get state = " + str(state))
    return response.json({"state": state}, status=200)


@app.route("/qsize", methods=['GET'])
async def qsize(request):
    """
    Return the size of the queue. When the producer is stopped, all incoming messages
    are stored in a queue and flushed as soon as its state switches to running.
    :param request:
    :return:
    """
    return response.json({"queue_size": queue.qsize()}, status=200)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="Producer.")
    parser.add_argument("-v", "--verbose", action="store_true", help="Increase output verbosity to debug level.")
    args = parser.parse_args()

    if args.verbose:
        logger.setLevel(logging.DEBUG)
    else:
        logger.setLevel(logging.INFO)

    if BROKERS:
        logger.info("Using BROKERS defined in env variables: {brokers}".format(brokers=BROKERS))
        brokers = BROKERS.split(',')
    else:
        brokers = 'localhost:9092'
        logger.info("Using default value for BROKERS: {brokers}".format(brokers=brokers))
    if TOPIC:
        logger.info("Using TOPIC defined in env variables: {topic}".format(topic=TOPIC))
        topic = TOPIC
    else:
        topic = 'kafka_test_topic'
        logger.info("Using default value for TOPIC: {topic}".format(topic=topic))
    if SERVICE_PORT:
        logger.info("Using SERVICE_PORT defined in env variables: {service_port}".format(service_port=SERVICE_PORT))
        service_port = SERVICE_PORT
    else:
        service_port = 9000
        logger.info("Using default value for SERVICE_PORT: {service_port}".format(service_port=service_port))

    server = app.create_server(host="0.0.0.0", port=service_port, access_log=False)
    task = asyncio.ensure_future(server)
    loop = asyncio.get_event_loop()
    producer = aiokafka.AIOKafkaProducer(loop=loop, bootstrap_servers=brokers, acks='all')
    loop.create_task(_process())
    signal(SIGINT, lambda s, f: loop.stop())
    try:
        loop.run_forever()
    except Exception:
        loop.create_task(producer.stop())
        loop.stop()
