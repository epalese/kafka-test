kafka-test
==========
This repository contains code and docker files to replicate a scenario with:  
- a Kafka cluster with 1 broker that supports a single topic stored in 4 different partitions  
- a service Producer that provides a `publish` endpoint to publish to the topic  
- a service Consumer1 that consumes data from the topic and logs the message on the standard output  
- a service Consumer2 that consumes data from the topic and stores the message on mongodb  
- an instance of Mongodb (used by Consumer2)

(optional)  
- a service Generator that simulate a random data genarator that contacts Producer in order to send data to the application.  

Installation
------------
Prerequisites:  
- docker  
- python 3.6  
- requests Python library  
- curl  

### 1. Clone the repository  
```bash
$ git clone https://epalese@bitbucket.org/epalese/kafka-test.git
```

### 2. Start containers  
We will start the services defined in `docker-compose.yml`:  
  
```bash
$ cd kafka-test
$ docker-compose up -d
Starting kafkatest_kafka_1 ... 
Starting kafkatest_mongodb_1 ... 
Starting kafkatest_producer_1 ... 
Starting kafkatest_kafka_1
Starting kafkatest_mongodb_1
Starting kafkatest_consumer1_1 ... 
Starting kafkatest_consumer1_1
Starting kafkatest_consumer2_1 ... 
Starting kafkatest_zookeeper_1 ... 
Starting kafkatest_producer_1
Starting kafkatest_zookeeper_1
Starting kafkatest_producer_1 ... done
```

You can check that everything is running with:
```bash
$ docker ps
CONTAINER ID        IMAGE                    COMMAND                  CREATED             STATUS              PORTS                                                NAMES
d8260e48c07f        wurstmeister/zookeeper   "/bin/sh -c '/usr/..."   25 minutes ago      Up 21 seconds       22/tcp, 2888/tcp, 3888/tcp, 0.0.0.0:2181->2181/tcp   kafkatest_zookeeper_1
8714cfe47120        bitnami/mongodb:latest   "/app-entrypoint.s..."   25 minutes ago      Up 22 seconds       0.0.0.0:27017->27017/tcp                             kafkatest_mongodb_1
a81d47eda26a        kafkatest_producer       "python producer_s..."   25 minutes ago      Up 21 seconds       0.0.0.0:9000->9000/tcp                               kafkatest_producer_1
1a2f3ee828e0        kafkatest_consumer1      "python consumer_s..."   25 minutes ago      Up 21 seconds       0.0.0.0:10001->10001/tcp                             kafkatest_consumer1_1
83d35f1aad9d        kafkatest_consumer2      "python consumer_s..."   25 minutes ago      Up 22 seconds       0.0.0.0:10002->10002/tcp                             kafkatest_consumer2_1
e11e41a76cf0        wurstmeister/kafka       "start-kafka.sh"         25 minutes ago      Up 23 seconds       0.0.0.0:9092->9092/tcp                               kafkatest_kafka_1
```  

And checking logs:
```bash
$ docker-compose logs -f
...
$ docker-compose logs -f kafka
...
$ docker-compose logs -f producer
...
$ docker-compose logs -f consumer1
```
As outlined before, the services that docker-compose will start are the following:  
- Kafka: Zooekeeper and Kafka broker supporting a topic `kafka_test_topic` that runs on 4 partitions.  
- MongoDB: containg a db named `kafka_test` that is supposed to contains a collection `messages` where Consumer2 stores its messages.  
- Producer: expose an endpoint `/publish` that can be used to push messages into the application.  
- Consumer1: consumes messages from Kafka logging them to the standard output.  
- Consumer2: consumes messages from Kafka logging them to the standard output and MongoDB.  

Note:  
Topic name and its partitions, IP ports, and other settings can be modified directly in docker-compose.yml.  

### 3. Checks  
If everything worked as intended you should see the following messages in the container logs:
```bash
$ docker-compose logs -f producer
...
producer_1   | [2018-01-08 12:07:57,019] 1-INFO producer_service::_process():l39: Checking for topic kafka_test_topic...
producer_1   | [2018-01-08 12:07:57,019] 1-INFO producer_service::_process():l42: Found topic with partitions {0, 1, 2, 3}
producer_1   | [2018-01-08 12:07:57,020] 1-INFO producer_service::_process():l46: Topic kafka_test_topic found.
producer_1   | [2018-01-08 12:07:57,020] 1-INFO producer_service::_process():l48: Starting producer...

$ docker-compose logs -f consumer1
...
consumer1_1  | [2018-01-08 12:08:26,797] 1-INFO consumer_model::setup_offset():l235: TopicPartition TopicPartition(topic='kafka_test_topic', partition=1): last committed message = None
consumer1_1  | [2018-01-08 12:08:26,797] 1-INFO consumer_model::setup_offset():l235: TopicPartition TopicPartition(topic='kafka_test_topic', partition=3): last committed message = None
consumer1_1  | [2018-01-08 12:08:26,797] 1-INFO consumer_model::setup_offset():l235: TopicPartition TopicPartition(topic='kafka_test_topic', partition=0): last committed message = None
consumer1_1  | [2018-01-08 12:08:26,797] 1-INFO consumer_model::setup_offset():l235: TopicPartition TopicPartition(topic='kafka_test_topic', partition=2): last committed message = None
consumer1_1  | [2018-01-08 12:08:26,798] 1-INFO consumer_model::run():l207: Start processing messages...

$ docker-compose logs -f consumer2
...
consumer2_1  | [2018-01-08 12:08:25,663] 1-INFO consumer_model::setup_offset():l201: TopicPartition TopicPartition(topic='kafka_test_topic', partition=1): last committed message stored in kafka = None
consumer2_1  | [2018-01-08 12:08:25,682] 1-INFO consumer_model::setup_offset():l201: TopicPartition TopicPartition(topic='kafka_test_topic', partition=2): last committed message stored in kafka = None
consumer2_1  | [2018-01-08 12:08:25,683] 1-INFO consumer_model::setup_offset():l201: TopicPartition TopicPartition(topic='kafka_test_topic', partition=0): last committed message stored in kafka = None
consumer2_1  | [2018-01-08 12:08:25,685] 1-INFO consumer_model::setup_offset():l201: TopicPartition TopicPartition(topic='kafka_test_topic', partition=3): last committed message stored in kafka = None
consumer2_1  | [2018-01-08 12:08:25,686] 1-INFO consumer_model::run():l104: Start processing messages...
```

Interactions
------------
### REST API
Consumers and producers have been implemented as microservices that expose a very basic REST API.  
If you have a look at `docker-compose.yml` you can see that Producer and Consumers expose a port to the local machine. In this way we can easily test
the API and interact with the microservices.

#### Producer  
**/state [GET, POST]**  
This endpoint allows to GET and POST (modify) the running state of the service. Allowed states: {"running", "stopped"}.  
```bash
$ curl -w '\n' -X GET localhost:9000/state
{"state":"running"}
$ curl -w '\n' -X POST "http://localhost:9000/state" -d '{"state":"stopped"}'
{"previous_state":"running","new_state":"stopped"}
$ curl -w '\n' -X POST "http://localhost:9000/state" -d '{"state":"running"}'
{"previous_state":"stopped","new_state":"running"}
```

**/qsize [GET]**  
The producer uses a queue to store incoming messages when its state is not running. This endpoint returns the current queue size:  
```bash
curl -w '\n' -X GET localhost:9000/qsize
{"queue_size":0}
```

**/publish [POST]**  
This endpoint can be called to send data to the application. 
The expected content is a JSON document with the following structure:
```json
{
    "id": "an id",
    "payload": "some payload"
}
```
```bash
$  curl -w '\n' -X POST "http://localhost:9000/publish" -d '{"id":"1", "payload":"test"}'
{"message":"ok"}
```

#### Consumer  
In the following example we are using localhost:10001 to interact with Consumer1 (the consumer whose action is just logging the message upon receival).
To interact with the other consumer service just replace hostname with localhost:10002.  

**/state [GET, POST]**  
This endpoint allows to GET and POST (modify) the running state of the service. Allowed states: {"running", "stopped"}.  
```bash
$ curl -w '\n' -X GET localhost:10001/state
{
   "state" : "running",
   "tp_state" : [
      {
         "partition" : 2,
         "topic" : "kafka_test_topic",
         "last_committed_offset" : null,
         "last_offset" : 0,
         "next_record_offset" : 0
      },
      {
         "last_committed_offset" : null,
         "topic" : "kafka_test_topic",
         "partition" : 0,
         "next_record_offset" : 0,
         "last_offset" : 0
      },
      {
         "next_record_offset" : 2,
         "last_offset" : 2,
         "last_committed_offset" : 2,
         "topic" : "kafka_test_topic",
         "partition" : 3
      },
      {
         "partition" : 1,
         "topic" : "kafka_test_topic",
         "last_committed_offset" : null,
         "last_offset" : 0,
         "next_record_offset" : 0
      }
   ]
}
```
As you can see from the command above, the state returned by the endpoint is:
- state: [running | stopped]
- tp_state: the state of all (topic, partition) assigned to the consumer
  * topic: the topic the consumer subscribed to
  * partition: the assigned partition
  * last_offset: it is the offset of the most recent message appeared in the partition
  * last_committed_offset: offset of the last message that has been committed
  * next_record_offset: offset of the next message that will be delivered to a consumer by Kafka

### Viewer Script
The repository comes with a script that queries producer and consumers at intervals and output their states.  
The scripts needs the list of hostnames for producers and consumers.    
```bash
$ python viewer.py --producers localhost:9000 --consumers localhost:10001,localhost:10002
```
Note: you need the requests library installed in your Python environment.    

From the output we can see that not that much is happening: 
```bash
$ python viewer.py --producers localhost:9000 --consumers localhost:10001,localhost:10002
[Producer:localhost:9000] 200	state: running	queue_size: 0
[Consumer:localhost:10001] 200	state: running
	(topic=kafka_test_topic; partition=2) offset_last_record: 0; last_committed_offset: None; next_record_offset: 0
	(topic=kafka_test_topic; partition=0) offset_last_record: 0; last_committed_offset: None; next_record_offset: 0
	(topic=kafka_test_topic; partition=3) offset_last_record: 4; last_committed_offset: 4; next_record_offset: 4
	(topic=kafka_test_topic; partition=1) offset_last_record: 0; last_committed_offset: None; next_record_offset: 0
[Consumer:localhost:10002] 200	state: running
	(topic=kafka_test_topic; partition=3) offset_last_record: 4; last_committed_offset: 4; next_record_offset: 4
	(topic=kafka_test_topic; partition=1) offset_last_record: 0; last_committed_offset: None; next_record_offset: 0
	(topic=kafka_test_topic; partition=2) offset_last_record: 0; last_committed_offset: None; next_record_offset: 0
	(topic=kafka_test_topic; partition=0) offset_last_record: 0; last_committed_offset: None; next_record_offset: 0

[Producer:localhost:9000] 200	state: running	queue_size: 0
[Consumer:localhost:10001] 200	state: running
	(topic=kafka_test_topic; partition=2) offset_last_record: 0; last_committed_offset: None; next_record_offset: 0
	(topic=kafka_test_topic; partition=0) offset_last_record: 0; last_committed_offset: None; next_record_offset: 0
	(topic=kafka_test_topic; partition=3) offset_last_record: 4; last_committed_offset: 4; next_record_offset: 4
	(topic=kafka_test_topic; partition=1) offset_last_record: 0; last_committed_offset: None; next_record_offset: 0
[Consumer:localhost:10002] 200	state: running
	(topic=kafka_test_topic; partition=3) offset_last_record: 4; last_committed_offset: 4; next_record_offset: 4
	(topic=kafka_test_topic; partition=1) offset_last_record: 0; last_committed_offset: None; next_record_offset: 0
	(topic=kafka_test_topic; partition=2) offset_last_record: 0; last_committed_offset: None; next_record_offset: 0
	(topic=kafka_test_topic; partition=0) offset_last_record: 0; last_committed_offset: None; next_record_offset: 0
```
  
We can can use the Generator service to hit the Producer endpoint with some random data.  
  
To see the different options that the generator script provides, run:    
```bash
$ python services/data_generator/generator.py --help
usage: generator.py [-h] [-e ENDPOINT] [-m MESSAGES] [-p PROCESSES] [-w WAIT]
                    [-v]

Requests generator.

optional arguments:
  -h, --help            show this help message and exit
  -e ENDPOINT, --endpoint ENDPOINT
                        Endpoint to call
  -m MESSAGES, --messages MESSAGES
                        (Optional) Number of messages per process to send.
  -p PROCESSES, --processes PROCESSES
                        Number of processes to spin up
  -w WAIT, --wait WAIT  Time to wait (ms) between each request
  -v, --verbose         Increase output verbosity to debug level
```

In a separate terminal screen, start the data generator, using 2 processes that send 1k messages (1 every 1 second):  
```bash
$ python services/data_generator/generator.py -v -e http://localhost:9000/publish --messages 10000 --processes 2 --wait 1000
2018-01-08 01:38:35 Starting process #0
2018-01-08 01:38:35 Starting process #1
2018-01-08 01:38:35 [200] msg = {"id": 422, "payload": "Payload for id = 422"} -> {"message":"ok"}
2018-01-08 01:38:35 [200] msg = {"id": 365876, "payload": "Payload for id = 365876"} -> {"message":"ok"}
2018-01-08 01:38:36 [200] msg = {"id": 945114, "payload": "Payload for id = 945114"} -> {"message":"ok"}
2018-01-08 01:38:36 [200] msg = {"id": 622555, "payload": "Payload for id = 622555"} -> {"message":"ok"}
2018-01-08 01:38:37 [200] msg = {"id": 717448, "payload": "Payload for id = 717448"} -> {"message":"ok"}
2018-01-08 01:38:37 [200] msg = {"id": 766686, "payload": "Payload for id = 766686"} -> {"message":"ok"}
```

Looking at the output of `viewer.py` you can see that messages start to flow and being processed by consumers:
```bash
[Producer:localhost:9000] 200	state: running	queue_size: 0
[Consumer:localhost:10001] 200	state: running
	(topic=kafka_test_topic; partition=2) offset_last_record: 0; last_committed_offset: None; next_record_offset: 0
	(topic=kafka_test_topic; partition=0) offset_last_record: 1; last_committed_offset: None; next_record_offset: 0
	(topic=kafka_test_topic; partition=3) offset_last_record: 6; last_committed_offset: 6; next_record_offset: 6
	(topic=kafka_test_topic; partition=1) offset_last_record: 1; last_committed_offset: 1; next_record_offset: 1
[Consumer:localhost:10002] 200	state: running
	(topic=kafka_test_topic; partition=3) offset_last_record: 7; last_committed_offset: 7; next_record_offset: 7
	(topic=kafka_test_topic; partition=1) offset_last_record: 1; last_committed_offset: 1; next_record_offset: 1
	(topic=kafka_test_topic; partition=2) offset_last_record: 1; last_committed_offset: 1; next_record_offset: 1
	(topic=kafka_test_topic; partition=0) offset_last_record: 1; last_committed_offset: 1; next_record_offset: 1

[Producer:localhost:9000] 200	state: running	queue_size: 0
[Consumer:localhost:10001] 200	state: running
	(topic=kafka_test_topic; partition=2) offset_last_record: 2; last_committed_offset: 2; next_record_offset: 2
	(topic=kafka_test_topic; partition=0) offset_last_record: 2; last_committed_offset: 1; next_record_offset: 1
	(topic=kafka_test_topic; partition=3) offset_last_record: 9; last_committed_offset: 9; next_record_offset: 9
	(topic=kafka_test_topic; partition=1) offset_last_record: 3; last_committed_offset: 3; next_record_offset: 3
[Consumer:localhost:10002] 200	state: running
	(topic=kafka_test_topic; partition=3) offset_last_record: 9; last_committed_offset: 9; next_record_offset: 9
	(topic=kafka_test_topic; partition=1) offset_last_record: 4; last_committed_offset: 3; next_record_offset: 3
	(topic=kafka_test_topic; partition=2) offset_last_record: 2; last_committed_offset: 2; next_record_offset: 2
	(topic=kafka_test_topic; partition=0) offset_last_record: 3; last_committed_offset: 3; next_record_offset: 3
```

### Docker-compose file with Generator service
The docker-compose file `docker-compose-with-generator.yml` starts the data generator service too (so there is no need
to start it manually from the terminal).  
To use it, run:
```bash 
$ docker-compose -f docker-compose-with-generator.yml up -d
```


Implementation details and design choices
-----------------------------------------
### AbstractConsumer
The exercise asked for:
> ...consumer service has a handler to either send messages to disk or to a datastore

I decided to provide a generic consumer through the AbstractConsumer abstract class. This class defines all the methods that any consumer
has to implement while providing the implementation for all common behaviours.
Different consumers can inherit from AbstractConsumer and provide different actions while processing records (eg: log messages,
store messages to disk or db, etc.).  

The interface of `consumer_model.AbstractConsumer` is:  
```python
consumer_model.AbstractConsumer():
    def get_state()
    def set_state(new_state)
    def get_kafka_consumer()
    async def stop()
    @abstractmethod async def setup_dependencies()
    async def connect_to_cluster(retries=0)
    async def subscribe_to_topic(retries=0)
    async def get_partition_assignment(retries=0):
    @abstractmethod async def setup_offset(assigned_partition)
    async def init()
    @abstractmethod async def process(data)
    async def run()
```

As you can see three methods are declared as abstract:  
- setup_dependencies: meant to be used for instantiating all the required additional dependencies, such as a connection to a db.  
- setup_offset: this method is supposed to be invoked after partitions has been assigned to the consumer and allows the consumer to decide the offset from where to start and process messages (this can be used in scenario where *exactly-once* semantic is needed).  
- process: this method is called for each record that the consumer fetches from the broked and it is supposed to implement the actual processing logic.  

Two concrete consumers have been provided:  
- LoggerConsumer  
- MongoConsumer  

The key differences between the two are:  
1. MongoConsumer stores its messages in a MongoDB collection while LoggerConsumer merely log them on the standard output.  
2. When MongoConsumer connects to the broker it uses the values stored in MongoDB to set up the offset for each partition.  

You can connect to the MongoDB instance and check the messages:  
```bash
$ mongo localhost:27017/kafka_test -u kafkauser -p dummy
MongoDB shell version: 3.2.5
connecting to: localhost:27017/kafka_test
> db.messages.find()
{ "_id" : ObjectId("5a5368f0ecfe3f00011ecbde"), "topic" : "kafka_test_topic", "partition" : 3, "offset" : 0, "key" : "1", "value" : "test" }
{ "_id" : ObjectId("5a5368feecfe3f00011ecbdf"), "topic" : "kafka_test_topic", "partition" : 3, "offset" : 1, "key" : "1", "value" : "test" }
{ "_id" : ObjectId("5a5373e9ecfe3f00011ecbe0"), "topic" : "kafka_test_topic", "partition" : 3, "offset" : 2, "key" : "163912", "value" : "Payload for id = 163912" }
{ "_id" : ObjectId("5a5373e9ecfe3f00011ecbe1"), "topic" : "kafka_test_topic", "partition" : 3, "offset" : 3, "key" : "225501", "value" : "Payload for id = 225501" }
{ "_id" : ObjectId("5a53745becfe3f00011ecbe2"), "topic" : "kafka_test_topic", "partition" : 3, "offset" : 4, "key" : "422", "value" : "Payload for id = 422" }
{ "_id" : ObjectId("5a53745becfe3f00011ecbe3"), "topic" : "kafka_test_topic", "partition" : 3, "offset" : 5, "key" : "365876", "value" : "Payload for id = 365876" }
{ "_id" : ObjectId("5a53745cecfe3f00011ecbe4"), "topic" : "kafka_test_topic", "partition" : 1, "offset" : 0, "key" : "945114", "value" : "Payload for id = 945114" }
{ "_id" : ObjectId("5a53745cecfe3f00011ecbe5"), "topic" : "kafka_test_topic", "partition" : 0, "offset" : 0, "key" : "622555", "value" : "Payload for id = 622555" }
{ "_id" : ObjectId("5a53745decfe3f00011ecbe6"), "topic" : "kafka_test_topic", "partition" : 3, "offset" : 6, "key" : "717448", "value" : "Payload for id = 717448" }
{ "_id" : ObjectId("5a53745decfe3f00011ecbe7"), "topic" : "kafka_test_topic", "partition" : 2, "offset" : 0, "key" : "766686", "value" : "Payload for id = 766686" }
{ "_id" : ObjectId("5a53745eecfe3f00011ecbe8"), "topic" : "kafka_test_topic", "partition" : 3, "offset" : 7, "key" : "894884", "value" : "Payload for id = 894884" }
{ "_id" : ObjectId("5a53745eecfe3f00011ecbe9"), "topic" : "kafka_test_topic", "partition" : 3, "offset" : 8, "key" : "651246", "value" : "Payload for id = 651246" }
{ "_id" : ObjectId("5a53745fecfe3f00011ecbea"), "topic" : "kafka_test_topic", "partition" : 1, "offset" : 1, "key" : "826080", "value" : "Payload for id = 826080" }
{ "_id" : ObjectId("5a53745fecfe3f00011ecbeb"), "topic" : "kafka_test_topic", "partition" : 2, "offset" : 1, "key" : "558511", "value" : "Payload for id = 558511" }
{ "_id" : ObjectId("5a537460ecfe3f00011ecbec"), "topic" : "kafka_test_topic", "partition" : 0, "offset" : 1, "key" : "131301", "value" : "Payload for id = 131301" }
{ "_id" : ObjectId("5a537460ecfe3f00011ecbed"), "topic" : "kafka_test_topic", "partition" : 1, "offset" : 2, "key" : "227317", "value" : "Payload for id = 227317" }
{ "_id" : ObjectId("5a537461ecfe3f00011ecbee"), "topic" : "kafka_test_topic", "partition" : 0, "offset" : 2, "key" : "19304", "value" : "Payload for id = 19304" }
{ "_id" : ObjectId("5a537461ecfe3f00011ecbef"), "topic" : "kafka_test_topic", "partition" : 1, "offset" : 3, "key" : "513461", "value" : "Payload for id = 513461" }
{ "_id" : ObjectId("5a537462ecfe3f00011ecbf0"), "topic" : "kafka_test_topic", "partition" : 1, "offset" : 4, "key" : "458895", "value" : "Payload for id = 458895" }
{ "_id" : ObjectId("5a537462ecfe3f00011ecbf1"), "topic" : "kafka_test_topic", "partition" : 3, "offset" : 9, "key" : "198469", "value" : "Payload for id = 198469" }
```

### Consumer groups
The two different specialization of AbstractConsumer can be used together with the consumer groups feature provided by Kafka allowing the implementation
of scenarios where messages in topic can be part of different application data flows.  
In fact, the deployment provided by the docker-composer files starts two different consumers that use different consumer classes (LoggerConsumer and
MongoConsumer). These two consumers are started in two separate consumer group ids, namely *LOGWRITER* and *DBWRITER* and each consume records
regardless of the other.  
  
Consumer group ids can be specified as environment variables in the docker-compose file. 

### Asynchronous Networking
I decided to experiment with Python asynchronous networking. The reasons is that it is supposed to be very fast: [https://magic.io/blog/uvloop-blazing-fast-python-networking/](https://magic.io/blog/uvloop-blazing-fast-python-networking/)  

So I built the stack using:  
- [Sanic](https://github.com/channelcat/sanic)  
- [aiokafka](http://aiokafka.readthedocs.io/en/stable/index.html)  

It seems quite a promising stack but I think it is still too young (no much documentation around) and testing asynchronous code it is not so straightforward. 

### Message serialization
The application is sending around textual JSON documents. In a high-load real scenario, this is far from being ideal. In a real scenario we need first to enable compression (Kafka brokers and clients support this natively).  
Also using a data serialization format (such Avro) can greatly help performances.  
Also some data serialization system allows to define schemas and check the data against the schema.
This can make easy to check that the structure of the message is conform to what expected.  
Also a framework like [marshmallow](https://marshmallow.readthedocs.io/en/latest/) can be used for the same goal.  

### Delivery semantic
The application tries to provide an at-least-once semantic. This guarantee is obtained committing the records once processed and forcing the consumers
to check the last committed record and start from the next one when they start. Also the producer use built-in facilities to resend messages in case of error.  
At-least-once semantic does not guarantee against duplicated messages dispatched to consumers.  
Exactly-once semantic can be achieved storing the offsets on the consumer-side and use them to move backward/forward on the Kafka partitions.  
`MongoConsumer` provides a similar mechanism where records are stored in MongoDB and when the MongoConsumer starts, it checks for the last committed
offsets and perform a `seek()` operation to move its pointers accordingly.  
