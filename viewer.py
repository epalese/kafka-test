import argparse
import json
import requests
import time


def query_service(endpoint):
    """
    Perform a GET request to the endpoint 'state' exposed by a consumer/producer service.
    :param endpoint: endpoint exposed by the service (eg: localhost:9001/state)
    :return: a tuple (response_code, state_info_dict)
    :rtype: tuple
    """
    try:
        r = requests.get(endpoint)
        return r.status_code, json.loads(r.text)
    except Exception as e:
        return "ERR", {'state': 'ERROR', 'queue_size': 'ERROR', 'error': str(e)}


def view(producers, consumers):
    """
    Print out the state of producers and consumers passed as arguments.
    Parameters are list of hostnames where the hostname has the format like: `hostname:port`.
    :param producers: list of producer hostnames
    :type producers: list
    :param consumers: list of consumer hostnames
    :type consumers: list
    :return: None
    """
    for producer in producers:
        state_endpoint = 'http://' + producer + '/state'
        queue_size_endpoint = 'http://' + producer + '/qsize'
        state_response = query_service(state_endpoint)
        queue_size_response = query_service(queue_size_endpoint)
        print("[Producer:{producer}] {status_code}\tstate: {state}\tqueue_size: {qsize}".format(
            producer=producer,
            status_code=state_response[0],
            state=state_response[1].get('state', None),
            qsize=queue_size_response[1].get('queue_size', None)
        ))
    for consumer in consumers:
        state_endpoint = 'http://' + consumer + '/state'
        state_response = query_service(state_endpoint)
        if state_response[0] == 200:
            print("[Consumer:{consumer}] {status_code}\tstate: {state}".format(
                consumer=consumer,
                status_code=state_response[0],
                state=state_response[1]['state']
            ))
            for tp_info in state_response[1]['tp_state']:
                topic = tp_info['topic']
                partition = tp_info['partition']
                offset_last_record = tp_info['last_offset']
                last_committed_offset = tp_info['last_committed_offset']
                next_record_offset = tp_info['next_record_offset']
                print("\t(topic={t}; partition={p}) offset_last_record: {olr}; last_committed_offset: {lcm}; next_record_offset: {nro}".format(
                    t=topic, p=partition,
                    olr=offset_last_record, lcm=last_committed_offset, nro=next_record_offset
                ))
        else:
            print("[Consumer:{consumer}] {status_code}\tstate: {state}".format(
                consumer=consumer,
                status_code=state_response[0],
                state=state_response[1]['state']
            ))
    print()


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="Query the state of producers and consumers.")
    parser.add_argument("-w", "--wait_time", help="Time (ms) to wait between each query.", action="store")
    parser.add_argument("-p", "--producers",
                        action="store",
                        help="Comma-separated list of producer host names. Ex: localhost:9000,localhost:9001",
                        required=True)
    parser.add_argument("-c", "--consumers",
                        action="store",
                        help="Comma-separated list of consumer host names. Ex: localhost:10001,localhost:10002",
                        required=True)
    args = parser.parse_args()
    producer_list = args.producers.split(',')
    consumer_list = args.consumers.split(',')
    wait_time = int(args.wait_time) if args.wait_time else 1000

    while True:
        view(producer_list, consumer_list)
        time.sleep(wait_time / 1000.0)